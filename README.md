# `angularjs-qr-scanner` — QRCode scanner in AngularJS

A QR Scanner example in AngularJS with feature to switch between front and rear camera on smartphone. 
This is the angular version of html5-qrcode and uses jsqrcode.

This AngularJS has been wrapped inside a nodeJS program, which allows us to deploy easily on `www.Heroku.com` or other kind of similar services.

### Install Dependencies

```
npm install
```

### Run the Application

We have preconfigured the project with a simple development web server. The simplest way to start
this server is:

```
npm start
```

Now browse to the app at [`localhost:80`].
